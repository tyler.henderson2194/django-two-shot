from django.shortcuts import render, redirect
from accounts.forms import AccountForm, AccountCreate
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User


def account_login(request):
    if request.method == "POST":
        form = AccountForm(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            user = authenticate(request, username=username, password=password)

            if user is not None:
                login(request, user)
                return redirect("home")
    else:
        form = AccountForm()

    context = {
        "form": form
    }

    return render(request, "accounts/login.html", context)


def account_logout(request):
    logout(request)
    return redirect("login")


def create_account(request):
    if request.method == "POST":
        form = AccountCreate(request.POST)

        if form.is_valid():
            username = form.cleaned_data["username"]
            password = form.cleaned_data["password"]
            password_confirmation = form.cleaned_data["password_confirmation"]

            if password == password_confirmation:
                user = User.objects.create_user(
                    username,
                    password=password
                )

                login(request, user)

                return redirect("home")
            else:
                form.add_error("Password", "Passwords do not match")
    else:
        form = AccountCreate()

    context = {
        "form": form
    }

    return render(request, "registration/signup.html", context)
