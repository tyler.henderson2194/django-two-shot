from django.urls import path
from accounts.views import account_login, account_logout, create_account

urlpatterns = [
    path("login/", account_login, name="login"),
    path("logout/", account_logout, name="logout"),
    path("signup/", create_account, name="signup"),
]
