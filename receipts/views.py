from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceiptForm, CreateExpenseForm, CreateAccountForm
from django.contrib.auth.decorators import login_required


@login_required
def show_receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipts": receipts
    }

    return render(request, "receipts/receipt_list.html", context)


@login_required
def create_new_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            new_receipt = form.save(False)
            new_receipt.purchaser = request.user
            new_receipt.save()
            return redirect("home")

    else:
        form = CreateReceiptForm

    context = {
        "form": form
    }

    return render(request, "receipts/create_receipt.html", context)


@login_required
def category_list(request):
    category = ExpenseCategory.objects.filter(owner=request.user)
    context = {
        "category": category
    }

    return render(request, "receipts/category_list.html", context)


@login_required
def account_list(request):
    account_info = Account.objects.filter(owner=request.user)
    context = {
        "account_info": account_info
    }

    return render(request, "receipts/accounts_list.html", context)


@login_required
def create_category(request):
    if request.method == "POST":
        form = CreateExpenseForm(request.POST)
        if form.is_valid():
            new_category = form.save(False)
            new_category.owner = request.user
            new_category.save()
            return redirect("category_list")

    else:
        form = CreateExpenseForm

    context = {
        "form": form
    }

    return render(request, "receipts/create_categories.html", context)


@login_required
def create_account(request):
    if request.method == "POST":
        form = CreateAccountForm(request.POST)
        if form.is_valid():
            new_account = form.save(False)
            new_account.owner = request.user
            new_account.save()
            return redirect("account_list")

    else:
        form = CreateAccountForm

    context = {
        "form": form
    }

    return render(request, "receipts/create_account.html", context)
